# Saltstack example
<img src="https://docs.saltproject.io/en/latest/_static/images/SaltStack_white.svg" alt="drawing" width="200">

This is a basic example that can help system administrators to start using Saltstack  



## Templates
- [User management](#user-management)

### User management
There are two pillars must be provided to allow flexible use of user managing and provisioning
- `pillar.users` - describes global user parameters like `account name`, `full name`, `ssh public key`, `user status` etc...
- `pillar.servers` - describes user to server bindings and server roles. Two user lists can be provided to server object: `server:users` and `server:admins` and each of item in those lists MUST be present in `pillar.users`



## Useful links
- Jinja Doc https://docs.saltproject.io/en/latest/topics/jinja/index.html
- States Doc https://docs.saltproject.io/en/latest/ref/states/all
